//
//  AppDelegate.m
//  FBAddressBook
//
//  Created by admin on 24.10.2018.
//  Copyright © 2018 admin. All rights reserved.
//

#import "AppDelegate.h"
#import "HomeViewController.h"

@import Firebase;

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [FIRApp configure];

    FIRUser *user = [FIRAuth auth].currentUser;

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *activeController = nil;
    //Проверяем авторизован пользователь или нет
    if (user) {
        activeController = [storyboard instantiateViewControllerWithIdentifier:@"HomeScene"];
    } else {
        activeController = [storyboard instantiateViewControllerWithIdentifier:@"LoginScene"];
    }

    UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController: activeController];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController = navController;
    [self.window makeKeyAndVisible];

    return YES;
}

@end
