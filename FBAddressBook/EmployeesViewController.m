//
//  EmployeesViewController.m
//  FBAddressBook
//
//  Created by admin on 08.11.2018.
//  Copyright © 2018 admin. All rights reserved.
//

#import "EmployeesViewController.h"

@import Firebase;

@interface EmployeesViewController ()
@property (weak, nonatomic) IBOutlet UILabel *employeesName;
@property (weak, nonatomic) IBOutlet UILabel *employeesTitle;
@property (weak, nonatomic) IBOutlet UIButton *phoneBottom;
@property (weak, nonatomic) IBOutlet UIButton *emailBottom;
@property (weak, nonatomic) IBOutlet UIImageView *employeesImage;

@end

@implementation EmployeesViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    UIBarButtonItem * backButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Выйти" style:UIBarButtonItemStylePlain target:self action:@selector(userLogout:)];
    self.navigationItem.rightBarButtonItem = backButtonItem;

    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://contact.taxsee.com/Contacts.svc/GetWPhoto?login=test_user&password=test_pass&id=%@",self.data.ID]];
    NSData *dataImg = [NSData dataWithContentsOfURL:url];
    UIImage *img = [[UIImage alloc] initWithData:dataImg];

    self.employeesImage.image = img;
    [self.employeesImage setContentMode:UIViewContentModeScaleAspectFit];
    self.employeesName.text = self.data.Name;
    self.employeesTitle.text = self.data.Title;
    [self.phoneBottom setTitle:self.data.Phone forState:UIControlStateNormal];
    [self.emailBottom setTitle:self.data.Email forState:UIControlStateNormal];
}

- (void)initWithEmployees:(Employees *)data {
    self.data = data;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)openCallPhone:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", self.data.Phone]] options:@{}
        completionHandler:^(BOOL success) {}];
}

- (IBAction)openSendEmail:(id)sender {
    #define URLEMail [NSString stringWithFormat:@"mailto:%@?subject=title&body=content", self.data.Email]
    NSString * url = [URLEMail stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    [[UIApplication sharedApplication]  openURL: [NSURL URLWithString: url] options:@{}
        completionHandler:^(BOOL success) {}];
}

- (IBAction)userLogout:(id)sender {
    NSError *signOutError;
    BOOL status = [[FIRAuth auth] signOut:&signOutError];
    if (!status) {
        NSLog(@"Error signing out: %@", signOutError);
        return;
    } else {
        self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
        UIViewController *activeController = [[UIStoryboard storyboardWithName:
            @"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"LoginScene"];
        UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController: activeController];
        
        self.window.rootViewController = navController;
        [self.window makeKeyAndVisible];
    }
}

@end
