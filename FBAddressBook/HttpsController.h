//
//  HttpsController.h
//  FBAddressBook
//
//  Created by admin on 11.10.2018.
//  Copyright © 2018 admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HttpsController : NSObject<NSURLSessionDownloadDelegate>

- (void)retrieveURL:(NSURL *)url successBlock:(void(^)(NSData *))successBlock;

@end
