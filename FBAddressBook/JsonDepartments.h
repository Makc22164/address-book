//
//  JsonDepartments.h
//  JsonDepartments
//
//  Created by admin on 05.12.2018.
//  Copyright © 2018 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JsonDepartments;
@class Departments;
@class Employees;

@interface JsonDepartments : NSObject

@property (nonatomic) NSMutableDictionary *JsonDepartments;

- (id)initWithJSONData: (NSDictionary*)data;
- (int)count;

@end

@interface Departments : NSObject

@property (copy, nonatomic) NSString *ID;
@property (copy, nonatomic) NSString *Name;
@property (strong, nonatomic) NSMutableDictionary *Departments;
@property (strong, nonatomic) NSMutableDictionary *Employees;

@end

@interface Employees : NSObject
@property (copy, nonatomic) NSString *ID;
@property (copy, nonatomic) NSString *Name;
@property (copy, nonatomic) NSString *Title;
@property (copy, nonatomic) NSString *Email;
@property (copy, nonatomic) NSString *Phone;
@end
