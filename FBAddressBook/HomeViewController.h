//
//  HomeViewController.h
//  FBAddressBook
//
//  Created by admin on 26.10.2018.
//  Copyright © 2018 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginViewController.h"
#import "JsonDepartments.h"

@interface HomeViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) UIWindow *window;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableDictionary *data;
@property (strong, nonatomic) NSString *UIBarTitleText;
@property (strong, nonatomic) NSMutableDictionary *tableDictionary;

@end
