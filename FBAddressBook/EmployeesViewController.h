//
//  EmployeesViewController.h
//  FBAddressBook
//
//  Created by admin on 08.11.2018.
//  Copyright © 2018 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JsonDepartments.h"

@interface EmployeesViewController : UIViewController

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) Employees *data;

- (void)initWithEmployees: (Employees*)data;
@end
