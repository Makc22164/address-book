//
//  JsonDepartments.m
//  JsonDepartments
//
//  Created by admin on 05.12.2018.
//  Copyright © 2018 admin. All rights reserved.
//

#import "JsonDepartments.h"

@implementation JsonDepartments

- (id)initWithJSONData: (NSDictionary*)data {
    self = [super init];
    if (self) {
        self.JsonDepartments = [[NSMutableDictionary alloc] init];
        self.JsonDepartments = [self getDepartments:data];
    }
    return self;
}

//получаем справочник
- (NSMutableDictionary*)getDepartments: (NSDictionary*)data {
    int key = 0;
    NSMutableDictionary *result = [[NSMutableDictionary alloc] init];
    for (NSDictionary *item in data) {
        if (item[@"Departments"] != NULL || item[@"Employees"] != NULL) {
            Departments *departaments = [[Departments alloc] init];
            departaments.ID = item[@"ID"];
            departaments.Name = item[@"Name"];

            if (item[@"Departments"] != NULL) {
                departaments.Departments = [self getDepartments:item[@"Departments"]];
            }

            if (item[@"Employees"] != NULL) {
                departaments.Employees = [self getEmployees:item[@"Employees"]];
            }

            [result setValue:departaments forKey:[NSString stringWithFormat: @"%d", key]];
            key++;
        }
    }
    return result;
}

- (NSMutableDictionary*)getEmployees: (NSDictionary*)data {
    int key = 0;
    NSMutableDictionary *result = [[NSMutableDictionary alloc] init];
    for (NSDictionary *item in data) {
        Employees *employees = [[Employees alloc] init];
        employees.ID = item[@"ID"];
        employees.Name = item[@"Name"];
        employees.Title = item[@"Title"];
        employees.Email = item[@"Email"];
        employees.Phone = item[@"Phone"];

        [result setValue:employees forKey:[NSString stringWithFormat: @"%d", key]];
        key++;
    }

    return result;
}

- (int)count {
    return (int)[self.JsonDepartments count];
}

@end

@implementation Departments
@end

@implementation Employees
@end
