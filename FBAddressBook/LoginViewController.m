//
//  LoginViewController.m
//  FBAddressBook
//
//  Created by admin on 26.10.2018.
//  Copyright © 2018 admin. All rights reserved.
//

#import "LoginViewController.h"

@import Firebase;

@interface LoginViewController () {
    UITextField *phoneField;
}
@property (weak, nonatomic) IBOutlet UITextField *codeField;
@property (weak, nonatomic) IBOutlet UIButton *getCodeButton;
@property (weak, nonatomic) IBOutlet UIButton *loginUserButton;

@end

@implementation LoginViewController
@synthesize codeField;

- (void)viewDidLoad {
    [super viewDidLoad];
    phoneField.layer.borderWidth = 1.0;
    codeField.layer.borderWidth = 1.0;
    self.getCodeButton.layer.borderWidth = 1.0;
    self.getCodeButton.contentEdgeInsets = (UIEdgeInsets){.top = 6, .left = 15, .bottom = 6, .right = 15};
    self.loginUserButton.layer.borderWidth = 1.0;
    self.loginUserButton.contentEdgeInsets = (UIEdgeInsets){.top = 6, .left = 15, .bottom = 6, .right = 15};

    //что бы каждый раз не вводить
    phoneField.text = @"+16505553434";
    //phoneField.text = @"+79195607345";
    self.codeField.text = @"123456";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

//запрос пароля
- (IBAction)getCode:(id)sender {
    NSString *_Nullable phone = phoneField.text;

    [[FIRPhoneAuthProvider provider] verifyPhoneNumber:phone
        UIDelegate:nil
        completion:^(NSString * _Nullable verificationID, NSError * _Nullable error) {
        if (error) {
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Error"
            message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                handler:^(UIAlertAction * action) {}];
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
            return;
        }

        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:verificationID forKey:@"authVerificationID"];
    }];
}

//авторизация пользователя
- (IBAction)loginUser:(id)sender {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *verificationID = [defaults stringForKey:@"authVerificationID"];
    NSString *_Nullable code = self.	codeField.text;
    FIRAuthCredential *credential = [[FIRPhoneAuthProvider provider]
        credentialWithVerificationID:verificationID
        verificationCode:code];

    [[FIRAuth auth] signInAndRetrieveDataWithCredential:credential
        completion:^(FIRAuthDataResult * _Nullable authResult,
            NSError * _Nullable error) {

                if (error) {
                    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Error"
                        message:[error localizedDescription]
                        preferredStyle:UIAlertControllerStyleAlert];

                    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                        handler:^(UIAlertAction * action) {}];
                    [alert addAction:defaultAction];
                    [self presentViewController:alert animated:YES completion:nil];
                    return;
                }

                if (authResult == nil) { return; }

                //загрузка сцены при удачной авторизации
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UIViewController *loadController = [storyboard instantiateViewControllerWithIdentifier:@"HomeScene"];

                UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController: loadController];
                self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
                self.window.rootViewController = navController;
                [self.window makeKeyAndVisible];
    }];
}

@end
