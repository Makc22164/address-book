//
//  HomeViewController.m
//  FBAddressBook
//
//  Created by admin on 26.10.2018.
//  Copyright © 2018 admin. All rights reserved.
//

#import "HomeViewController.h"
#import "HttpsController.h"
#import "JsonDepartments.h"
#import "EmployeesViewController.h"

@import Firebase;

@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    FIRUser *user = [FIRAuth auth].currentUser;
    if (user && self.data == nil) {
        [self getTree];
    }

    self.tableView.dataSource = self;
    self.tableView.delegate = self;

    UIBarButtonItem * backButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Выйти" style:UIBarButtonItemStylePlain target:self action:@selector(userLogout:)];
    self.navigationItem.rightBarButtonItem = backButtonItem;
    if([self.UIBarTitleText length] != 0) {
        UILabel *UIBarTitle = [[UILabel alloc] init];
        UIBarTitle.text = self.UIBarTitleText;
        self.navigationItem.titleView = UIBarTitle;

        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:self.UIBarTitleText style:UIBarButtonItemStylePlain target:nil action:nil];
        self.navigationItem.backBarButtonItem = backButton;
    }
}

//Загрузка адресной книги из json
- (void)getTree {
    HttpsController *http = [[HttpsController alloc] init];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://contact.taxsee.com/Contacts.svc/GetAll?login=test_user&password=test_pass"]];

    [http retrieveURL:url successBlock:^(NSData *response){
        NSError *error = nil;

        // проверяем является ли ответ json ом
        NSDictionary *data = [NSJSONSerialization JSONObjectWithData:response
            options:0
            error:&error];
        if (!error) {
            JsonDepartments *model = [[JsonDepartments alloc] initWithJSONData:data[@"Departments"]];
            self.data = model.JsonDepartments;
        } else {
            NSLog(@"error - %@", error);
        }

        [self.tableView reloadData];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.data count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Category";

    //проверяем какой класс данных
    if([[self.data objectForKey:[NSString stringWithFormat: @"%d", (int)indexPath.row]] isKindOfClass:[Departments class]]) {
        Departments *item = [self.data objectForKey:[NSString stringWithFormat: @"%d", (int)indexPath.row]];
        cellIdentifier = @"Category";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: cellIdentifier];
        cell.textLabel.text = item.Name;
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%d", (int)indexPath.row +1];

        return cell;
    } else {
        Employees *item = [self.data objectForKey:[NSString stringWithFormat: @"%d", (int)indexPath.row]];
        cellIdentifier = @"Employees";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: cellIdentifier];
        cell.textLabel.text = item.Name;
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%d", (int)indexPath.row +1];

        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if([[self.data objectForKey:[NSString stringWithFormat: @"%d", (int)indexPath.row]] isKindOfClass:[Departments class]]) {
        Departments *item = [self.data objectForKey:[NSString stringWithFormat: @"%d", (int)indexPath.row]];
        HomeViewController *homeController = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeScene"];

        if (item.Departments != NULL) {
            homeController.data = item.Departments;
            homeController.UIBarTitleText = item.Name;
            [self.navigationController pushViewController:homeController animated:YES];
        } else if (item.Employees != NULL) {
            homeController.data = item.Employees;
            homeController.UIBarTitleText = item.Name;
            [self.navigationController pushViewController:homeController animated:YES];
        }
    } else {
        Employees *item = [self.data objectForKey:[NSString stringWithFormat: @"%d", (int)indexPath.row]];
        EmployeesViewController *employeesController = [self.storyboard instantiateViewControllerWithIdentifier:@"EmployeesScene"];
        [employeesController initWithEmployees:item];
        [self.navigationController pushViewController:employeesController animated:YES];
    }
}

- (IBAction)userLogout:(id)sender {
    NSError *signOutError;
    BOOL status = [[FIRAuth auth] signOut:&signOutError];
    if (!status) {
        NSLog(@"Error signing out: %@", signOutError);
        return;
    } else {
        self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
        UIViewController *activeController = [[UIStoryboard storyboardWithName:
            @"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"LoginScene"];
        UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController: activeController];

        self.window.rootViewController = navController;
        [self.window makeKeyAndVisible];
    }
}

@end
